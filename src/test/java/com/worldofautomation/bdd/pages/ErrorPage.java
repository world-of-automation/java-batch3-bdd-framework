package com.worldofautomation.bdd.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class ErrorPage {
    @FindBy(linkText = "Log In")
    private WebElement logIn;

    public void validateLoginIsDisplayed() {
        Assert.assertTrue(logIn.isDisplayed());
    }
}
