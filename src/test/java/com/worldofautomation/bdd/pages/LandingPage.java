package com.worldofautomation.bdd.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LandingPage {

    @FindBy(xpath = "//i[@class='fb_logo img sp_iweMvYcNOXG sx_c594d5']")
    private WebElement fbLogo;

    @FindBy(linkText = "Sign Up")
    private WebElement signUp;

    @FindBy(linkText = "Log In")
    private WebElement logIn;

    @FindBy(id = "pass")
    private WebElement pass;

    @FindBy(id = "email")
    private WebElement email;

    public void validateFbLogoIsDisplayed() {
        System.out.println(fbLogo.isDisplayed());
    }

    public void validateFootersAreDisplayed() {
        System.out.println(signUp.isDisplayed());
        System.out.println(logIn.isDisplayed());
    }

    public void clickOnSignUp() {
        signUp.click();
    }

    public void clickOnLogIn() {
        signUp.click();
    }

    public void fillUserNameAndPass(String user, String passss) {
        email.sendKeys(user);
        pass.sendKeys(passss);
    }
}
