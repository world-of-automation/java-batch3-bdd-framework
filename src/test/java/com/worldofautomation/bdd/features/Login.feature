Feature: login functionality features

# valid username valid pass

  Background:
    Given  user opens the browser and navigate to facebook login page
    When user can see the facebook logo is displayed

  @Debug
  Scenario Outline: user not being able to login using invalid pass and valid username
    Then user provider <username> and <password> in the desired field
    And user clicks on the login button
    And user validates error page has been displayed
    And user closes the browser


    Examples:
      | username                    | password |
      | testuser00133@gmail.com     | 234rwcb  |
      | testuser034533@gmail.com    | 5t3r24   |
      | testuser033563654@gmail.com | 4564ge   |


  @Debug
  Scenario: user being able to click on each of the footer of the homepage and navigate back
    Then user validate every footers options is displayed
    And  user clicks on each of the footers and navigates back
    And user closes the browser



  # VALID sername insvalid pss

  # invalid user, invl pass

  #pass encryption

  #email and pass field
