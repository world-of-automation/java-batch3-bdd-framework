package com.worldofautomation.bdd.stepdef;

import com.worldofautomation.bdd.TestBase;
import com.worldofautomation.bdd.pages.LandingPage;
import cucumber.api.java.en.Then;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

public class LoginFunctionality extends TestBase {
    private static final Logger logger = Logger.getLogger(LoginFunctionality.class);
    LandingPage landingPage = null;

    @Then("^user provider (.*) and (.*) in the desired field$")
    public void user_provider_username_and_password_in_the_desired_field(String user, String pass) {
        landingPage = PageFactory.initElements(getDriver(), LandingPage.class);
        landingPage.fillUserNameAndPass(user, pass);
        logger.info("username is : " + user);
        logger.info("password is : " + pass);
    }

    @Then("^user clicks on the login button$")
    public void user_clicks_on_the_login_button() {
        landingPage.clickOnLogIn();
        System.out.println("passed1");
    }

    @Then("^user validates error page has been displayed$")
    public void user_validates_error_page_has_been_displayed() {
        System.out.println("passed2");
        /*sleepFor(20);
        System.out.println("passed3");
        String url = getCurrentUrl();
        System.out.println("passed4");
        Assert.assertTrue(url.contains("login_attempt"));
        ErrorPage errorPage = PageFactory.initElements(getDriver(), ErrorPage.class);
        errorPage.validateLoginIsDisplayed();*/
    }
}
