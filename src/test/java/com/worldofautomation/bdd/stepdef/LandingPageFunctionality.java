package com.worldofautomation.bdd.stepdef;

import com.worldofautomation.bdd.TestBase;
import com.worldofautomation.bdd.pages.LandingPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class LandingPageFunctionality {

    private static final Logger logger = Logger.getLogger(LandingPageFunctionality.class);

    private LandingPage landingPage = null;

    @Given("^user opens the browser and navigate to facebook login page$")
    public void user_opens_the_browser_and_navigate_to_facebook_login_page() {
        TestBase.launchBrowser("chrome");
        TestBase.getDriver().get("https://www.facebook.com/");
        logger.info("Browser opened asnd facebook loaded");
        TestBase.getDriver().manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
    }

    @When("^user can see the facebook logo is displayed$")
    public void user_can_see_the_facebook_logo_is_displayed() {
        landingPage = PageFactory.initElements(TestBase.getDriver(), LandingPage.class);
        landingPage.validateFbLogoIsDisplayed();
        logger.info("FB LOGO IS DISPLAYED");
    }

    @Then("^user validate every footers options is displayed$")
    public void user_validate_every_footers_options_is_displayed() {
        landingPage.validateFootersAreDisplayed();
        logger.info("FB FOOTERS are DISPLAYED");
    }

    @Then("^user clicks on each of the footers and navigates back$")
    public void user_clicks_on_each_of_the_footers_and_navigates_back() {
        landingPage.clickOnSignUp();
        logger.info("Sign Up Button Clicked");
        TestBase.getDriver().navigate().back();
        landingPage.clickOnLogIn();
    }

    @Then("^user closes the browser$")
    public void user_closes_the_browser() {
        TestBase.getDriver().quit();
        logger.info("driver closed");
    }


    @Then("^user validates different languages and clicks on them$")
    public void user_validates_different_languages_and_clicks_on_them() {

    }


}
